<?xml version="1.0" encoding="UTF-8"?>
<Wix xmlns="http://schemas.microsoft.com/wix/2006/wi">
	<Product Id="*" Name="PyTangoArchivingInstaller" Language="1033" Version="1.0.0.0" Manufacturer="3Controls" UpgradeCode="28ce502e-86eb-41f8-a04b-e097a5eddb60">
		<Package InstallerVersion="200" Compressed="yes" InstallScope="perMachine" />

		<MajorUpgrade DowngradeErrorMessage="A newer version of [ProductName] is already installed." />
		<MediaTemplate />

		<Feature Id="ProductFeature" Title="PyTangoArchivingInstaller" Level="1">
			<ComponentGroupRef Id="ProductComponents" />
		</Feature>
	</Product>

	<Fragment>
		<Directory Id="TARGETDIR" Name="SourceDir">
			<Directory Id="ProgramFilesFolder">
				<Directory Id="INSTALLFOLDER" Name="PyTangoArchivingInstaller" />
			</Directory>
      <Directory Id ="ApplicationsProgramFolder" Name="simpleAppStartMenu">
        
      </Directory>
		</Directory>
	</Fragment>

	<Fragment>
    <ComponentGroup Id="ProductComponents" Directory="INSTALLFOLDER">
      <Component Id="ProductComponent" Guid="1BF4A220-AC45-46CE-9206-C1809795EAA2">
        <File Source="PyTangoArchiving-7.3.2.win-amd64.exe" />
      </Component>

    </ComponentGroup>
      
      
      		<!-- TODO: Remove the comments around this Component element and the ComponentRef below in order to add resources to this installer. -->
			<!-- <Component Id="ProductComponent"> -->
				<!-- TODO: Insert files, registry keys, and other resources here. -->
			<!-- </Component> -->
      
		
	</Fragment>
</Wix>
